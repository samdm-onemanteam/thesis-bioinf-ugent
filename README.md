# Assessing the performance of network crosstalk analysis combined with clustering

This is the text for my master thesis at Ghent University

Compiling is only possible on Linux systems with LuaLaTeX and the calibri and verdana fonts installed.

Compiling with latexmk is possible using the lualatex option, but if CMake is available, then a cmake build is also possible and recommended. Compiling for the
first time might take a minute or two, because system fonts need to be converted to LaTeX compatible fonts first.

## compile pdf

```
mkdir target
cd target
cmake ..
make
```