setwd("~/Documents/1_UGent/5_Uma2/2S-thesis/Investigate-pval-msize/Figures/")
library(readr)
library(ggplot2)
library(foreach)
library(cowplot)
theme_set(theme_gray())

log10breaks <- function(min, max, l = 5, roundto = 0) {
  round(10^(seq(min, log10(max), length.out = l)), roundto)
}
fractionbreaks <- function(base, howmany = 20) {
  1/(base^(0:howmany))
}
fractionlabels <- function(base, howmany = 20) {
  paste(1, base^(0:howmany), sep = "/")
}

thetheme <- theme(
  panel.background = element_blank(),
  panel.grid.minor = element_blank(),
  panel.grid.major = element_blank(),
  plot.background = element_blank(),
  axis.text.x = element_text(angle = 45, hjust = 1)
)

# FP plots
makeFPplot <- function(thedata, minx, maxx = 1.5, maxy,
                       countbreaks,
                       fracbreaks = fractionbreaks(2),
                       fraclabels = fractionlabels(2),
                       stat = "adjPVal",
                       hjust = 0) {

  thedata <- subset(thedata, get(stat) <= maxy)
  ggplot(thedata, aes(x = sizeA/totsize, y = get(stat))) +
    geom_hex(bins = 40) +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaks,
                         colours = topo.colors(5)) +
    scale_x_continuous(trans = "log2",
                       limits = c(minx, maxx),
                       labels = fraclabels,
                       breaks = fracbreaks) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("nodes in module/nodes in graph") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95 + hjust, 0.5)) +
    thetheme
}

makeFPplot.nodegr <- function(thedata, minx, maxx = 1.5, maxy, countbreaks, stat = "adjPVal") {
  thedata <- subset(thedata, get(stat) <= maxy)
  ggplot(thedata, aes(x = CB.groupLinkDegree.first/sizeA, y = get(stat))) +
    geom_hex(bins = 40) +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaks,
                         colours = topo.colors(5)) +
   scale_x_continuous(trans = "log2",
                      breaks = c(0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512),
                      limits = c(minx, maxx)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("average node degree in module") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.935, 0.5)) +
    thetheme
}

# TP plots
makeTPplot <- function(thedata, minx, maxx = 2, maxy, countbreaksa, countbreaksb, hjust, stat = "adjPVal") {
  thedata <- subset(thedata, get(stat) <= maxy)
  p1 <- ggplot(thedata, aes(x = sizeA/totsize, y = get(stat))) +
    geom_hex(bins = 40, alpha = 0.30) +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksa,
                         colours = topo.colors(5)) +
    scale_x_continuous(trans = "log2",
                       limits = c(minx, maxx),
                       labels = fractionlabels(2),
                       breaks = fractionbreaks(2)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("nodes in module/nodes in graph") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95, 0.30)) +
    thetheme
  posdata <- subset(thedata, istarget)
  p2 <- ggplot(posdata, aes(x = sizeA/totsize, y = get(stat))) +
    geom_hex(bins = 40, colour = "black") +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksb,
                         colours = heat.colors(5)) +
    scale_x_continuous(trans = "log2",
                       limits = c(minx, maxx),
                       labels = fractionlabels(2),
                       breaks = fractionbreaks(2)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("nodes in module/nodes in graph") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95 + hjust, 0.75)) +
    thetheme

  ggdraw() + draw_plot(p1) + draw_plot(p2)
}

makeTPplot.nodegr <- function(thedata, minx, maxx = 2, maxy, countbreaksa, countbreaksb, hjust, stat = "adjPVal") {
  thedata <- subset(thedata, get(stat) <= maxy)
  p1 <- ggplot(thedata, aes(x = CB.groupLinkDegree.first/sizeA, y = get(stat))) +
    geom_hex(bins = 40, alpha = 0.30) +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksa,
                         colours = topo.colors(5)) +
     scale_x_continuous(trans = "log2",
     breaks = c(0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024),
     limits = c(minx, maxx)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("average node degree in module") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95, 0.30)) +
    thetheme
  posdata <- subset(thedata, istarget)
  p2 <- ggplot(posdata, aes(x = CB.groupLinkDegree.first/sizeA, y = get(stat))) +
    geom_hex(bins = 40, colour = "black") +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksb,
                         colours = heat.colors(5)) +
     scale_x_continuous(trans = "log2",
     breaks = c(0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024),
     limits = c(minx, maxx)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("average node degree in module") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95 + hjust, 0.75)) +
    thetheme
  ggdraw() + draw_plot(p1) + draw_plot(p2)
}

makeTPplot.overlap <- function(thedata, minx, maxx = 10, maxy, countbreaksa, countbreaksb, hjust, stat = "adjPVal") {
  thedata <- subset(thedata, get(stat) <= maxy)
  p1 <- ggplot(thedata, aes(x = EASE.overlap, y = get(stat))) +
    geom_hex(bins = 40, alpha = 0.30) +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksa,
                         colours = topo.colors(5)) +
     scale_x_continuous(limits = c(minx, maxx)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("overlap between pathway and module") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95, 0.30)) +
    thetheme
  posdata <- subset(thedata, istarget)
  p2 <- ggplot(posdata, aes(x = EASE.overlap, y = get(stat))) +
    geom_hex(bins = 40, colour = "black") +
    scale_fill_gradientn(name = NULL,
                         trans = "log1p",
                         breaks = countbreaksb,
                         colours = heat.colors(5)) +
     scale_x_continuous(limits = c(minx, maxx)) +
    scale_y_continuous(limits = c(0 - 1e-3, maxy + 1e-3)) +
    xlab("overlap between pathway and module") +
    ylab(switch (stat,
      "adjPVal" = "adjusted p-value",
      "pVal" = "p-value",
      stop("???")
    )) +
    theme(legend.position = c(0.95 + hjust, 0.75)) +
    thetheme
  ggdraw() + draw_plot(p1) + draw_plot(p2)
}

makeTPplot.scatter <- function(thedata, maxy, stat = "adjPVal") {
  thedata$istargetF <- factor(thedata$istarget, levels = c("TRUE", "FALSE"))
  thedata <- subset(thedata, get(stat) <= maxy)
  ggplot(thedata, aes(x = sizeA/totsize, y = get(stat), colour = istargetF, size = istargetF, alpha = istargetF)) +
    geom_point() +
  scale_color_manual(values=c("red", "black")) +
  scale_size_manual(values = c(2, 1)) +
  scale_alpha_manual(values = c(1, 0.4)) +
    thetheme +
    theme(panel.border = element_rect(fill = NA, colour = "black", size = 1),
          legend.position = "none") +
  xlab("nodes in module/nodes in graph") +
  ylab(switch (stat,
    "adjPVal" = "adjusted p-value",
    "pVal" = "p-value",
    stop("???")
  ))
}

makeTPplot.overlap.scatter <- function(thedata, maxy, stat = "adjPVal") {
  thedata$istargetF <- factor(thedata$istarget, levels = c("TRUE", "FALSE"))
  thedata <- subset(thedata, get(stat) <= maxy)
  ggplot(thedata, aes(x = EASE.overlap, y = get(stat), colour = istargetF, size = istargetF, alpha = istargetF)) +
    geom_point() +
  scale_color_manual(values=c("red", "black")) +
  scale_size_manual(values = c(2, 1)) +
  scale_alpha_manual(values = c(1, 0.4)) +
    thetheme +
    theme(panel.border = element_rect(fill = NA, colour = "black", size = 1),
          legend.position = "none") +
  xlab("overlap between pathway and module") +
  ylab(switch (stat,
    "adjPVal" = "adjusted p-value",
    "pVal" = "p-value",
    stop("???")
  ))
}

#####
if (F) {
  # overlap plots
  Binox_mcl_FP <- read_tsv("Binox_mcl_FP")
  (Binox_mcl_FP.plot <- makeFPplot(Binox_mcl_FP, minx = 1/2100, maxx = 3, maxy = 0.05,
                                   countbreaks = log10breaks(0, 16e3)))
  Binox_mgc_FP <- read_tsv("Binox_mgc_FP")
  (Binox_mgc_FP.plot <- makeFPplot(Binox_mgc_FP, minx = 1/2100, maxx = 1, maxy = 0.05,
                                   countbreaks = log10breaks(0, 16e3),
                                   fracbreaks = fractionbreaks(2)[3:20],
                                   fraclabels = fractionlabels(2)[3:20],
                                   hjust = -0.01))
  Binox_mcl_FP_geneperm <- read_tsv("Binox_mcl_FP_geneperm")
  (Binox_mcl_FP_geneperm.plot <- makeFPplot(Binox_mcl_FP_geneperm, minx = 1/2500, maxx = 3, maxy = 0.05,
                                   countbreaks = log10breaks(0, 16e3)))
  Binox_mgc_FP_geneperm <- read_tsv("Binox_mgc_FP_geneperm")
  (Binox_mgc_FP_geneperm.plot <- makeFPplot(Binox_mgc_FP_geneperm, minx = 1/2100, maxx = 1, maxy = 0.05,
                                   countbreaks = log10breaks(0, 16e3),
                                   fracbreaks = fractionbreaks(2)[3:20],
                                   fraclabels = fractionlabels(2)[3:20]))

  Binox_mcl_TP <- read_tsv("Binox_mcl_TP")
  (Binox_mcl_TP.plot <- makeTPplot(Binox_mcl_TP, minx = 1/2100, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.024))
  Binox_mgc_TP <- read_tsv("Binox_mgc_TP")
  (Binox_mgc_TP.plot <- makeTPplot(Binox_mgc_TP, minx = 1/2100, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.024))
  Binox_mcl_TP_MSigDb <- read_tsv("Binox_mcl_TP_MSigDb")
  (Binox_mcl_TP_MSigDb.plot <- makeTPplot(Binox_mcl_TP_MSigDb, minx = 1/190, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.024))
  Binox_mgc_TP_MSigDb <- read_tsv("Binox_mgc_TP_MSigDb")
  (Binox_mgc_TP_MSigDb.plot <- makeTPplot(Binox_mgc_TP_MSigDb, minx = 1/190, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.024))

  Ease_mcl_FP <- read_tsv("Ease_mcl_FP")
  (Ease_mcl_FP.plot <- makeFPplot(Ease_mcl_FP, minx = 1/2100, maxx = 2, maxy = 0.20,
                                 countbreaks = log10breaks(0, 250)))
  Ease_mgc_FP <- read_tsv("Ease_mgc_FP")
  (Ease_mgc_FP.plot <- makeFPplot(Ease_mgc_FP, minx = 1/2500, maxx = 1, maxy = 0.20,
                                 countbreaks = log10breaks(0, 250),
                                   fracbreaks = fractionbreaks(2)[2:20],
                                   fraclabels = fractionlabels(2)[2:20],
                                   hjust = 0.0))
  Ease_mcl_FP_geneperm <- read_tsv("Ease_mcl_FP_geneperm")
  (Ease_mcl_FP_geneperm.plot <- makeFPplot(Ease_mcl_FP_geneperm, minx = 1/2100, maxx = 2, maxy = 0.20,
                                 countbreaks = log10breaks(0, 250)))
  Ease_mgc_FP_geneperm <- read_tsv("Ease_mgc_FP_geneperm")
  (Ease_mgc_FP_geneperm.plot <- makeFPplot(Ease_mgc_FP_geneperm, minx = 1/2500, maxx = 1, maxy = 0.20,
                                 countbreaks = log10breaks(0, 250),
                                   fracbreaks = fractionbreaks(2)[3:20],
                                   fraclabels = fractionlabels(2)[3:20],
                                   hjust = 0.0))

  Ease_mcl_TP <- read_tsv("Ease_mcl_TP")
  (Ease_mcl_TP.plot <- makeTPplot(Ease_mcl_TP, minx = 1/2100, maxy = 0.20,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008))
  Ease_mgc_TP <- read_tsv("Ease_mgc_TP")
  (Ease_mgc_TP.plot <- makeTPplot(Ease_mgc_TP, minx = 1/2100, maxy = 0.25,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008))
  Ease_mcl_TP_MSigDb <- read_tsv("Ease_mcl_TP_MSigDb")
  (Ease_mcl_TP_MSigDb.plot <- makeTPplot(Ease_mcl_TP_MSigDb, minx = 1/140, maxy = 0.20,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008))
  Ease_mgc_TP_MSigDb <- read_tsv("Ease_mgc_TP_MSigDb")
  (Ease_mgc_TP_MSigDb.plot <- makeTPplot(Ease_mgc_TP_MSigDb, minx = 1/140, maxy = 0.20,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008))

  save_plot("Binox_mcl_FP.pdf", Binox_mcl_FP.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_FP.pdf", Binox_mgc_FP.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mcl_FP_geneperm.pdf", Binox_mcl_FP_geneperm.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_FP_geneperm.pdf", Binox_mgc_FP_geneperm.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mcl_TP.pdf", Binox_mcl_TP.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_TP.pdf", Binox_mgc_TP.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mcl_TP_MSigDb.pdf", Binox_mcl_TP_MSigDb.plot, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_TP_MSigDb.pdf", Binox_mgc_TP_MSigDb.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mcl_FP.pdf", Ease_mcl_FP.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_FP.pdf", Ease_mgc_FP.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mcl_FP_geneperm.pdf", Ease_mcl_FP_geneperm.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_FP_geneperm.pdf", Ease_mgc_FP_geneperm.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mcl_TP.pdf", Ease_mcl_TP.plot, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_TP.pdf", Ease_mgc_TP.plot, base_width = 5, base_height = 4)

  # node degree plots
  (Binox_mcl_FP.plot.nodegr <- makeFPplot.nodegr(Binox_mcl_FP, minx = 0.9, maxx = 1800, maxy = 0.05,
                                   countbreaks = log10breaks(0, 16e3)))

  (Binox_mcl_TP.plot.nodegr <- makeTPplot.nodegr(Binox_mcl_TP, minx = 0.9, maxx = 1000, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.015))
  (Binox_mgc_TP.plot.nodegr <- makeTPplot.nodegr(Binox_mgc_TP, minx = 0.9, maxx = 4000, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.015))
  (Binox_mcl_TP_MSigDb.plot.nodegr <- makeTPplot.nodegr(Binox_mcl_TP_MSigDb, minx = 1, maxx = 4000, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.015))
  (Binox_mgc_TP_MSigDb.plot.nodegr <- makeTPplot.nodegr(Binox_mgc_TP_MSigDb, minx = 1, maxx = 4000, maxy = 0.05,
             countbreaksa = log10breaks(0, 16e3),
             countbreaksb = log10breaks(0, 16), hjust = -0.015))

  save_plot("Binox_mcl_FP_nodegree.pdf", Binox_mcl_FP.plot.nodegr, base_width = 5, base_height = 4)
  save_plot("Binox_mcl_TP_nodegree.pdf", Binox_mcl_TP.plot.nodegr, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_TP_nodegree.pdf", Binox_mgc_TP.plot.nodegr, base_width = 5, base_height = 4)
  save_plot("Binox_mcl_TP_MSigDb_nodegree.pdf", Binox_mcl_TP_MSigDb.plot.nodegr, base_width = 5, base_height = 4)
  save_plot("Binox_mgc_TP_MSigDb_nodegree.pdf", Binox_mgc_TP_MSigDb.plot.nodegr, base_width = 5, base_height = 4)

  # Overlap plots
  (Ease_mcl_TP.plot.overlap <- makeTPplot.overlap(Ease_mcl_TP, minx = 0, maxx = 75, maxy = 0.20,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008))
  (Ease_mgc_TP.plot.overlap <- makeTPplot.overlap(Ease_mgc_TP, minx = 0, maxx = 30, maxy = 0.25,
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 2), hjust = -0.008)) # doesn't look nice

  save_plot("Ease_mcl_TP_overlap.pdf", Ease_mcl_TP.plot.overlap, base_width = 5, base_height = 4)

  # Ease scatterplots
  (Ease_mgc_TP.plot.scatter <- makeTPplot.scatter(Ease_mgc_TP, 0.05))
  (Ease_mgc_TP.plot.overlap.scatter <- makeTPplot.overlap.scatter(Ease_mgc_TP, 0.05))

  save_plot("Ease_mgc_TP_scatter.pdf", Ease_mgc_TP.plot.scatter, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_TP_overlap_scatter.pdf", Ease_mgc_TP.plot.overlap.scatter, base_width = 5, base_height = 4)

  # MSigDb scatterplots
  (Ease_mcl_TP_MSigDb.plot.scatter <- makeTPplot.scatter(Ease_mcl_TP_MSigDb, 0.05))
  (Ease_mgc_TP_MSigDb.plot.scatter <- makeTPplot.scatter(Ease_mgc_TP_MSigDb, 0.05))

  (Ease_mcl_TP_MSigDb.plot.overlap.scatter <- makeTPplot.overlap.scatter(Ease_mcl_TP_MSigDb, 0.05))
  (Ease_mgc_TP_MSigDb.plot.overlap.scatter <- makeTPplot.overlap.scatter(Ease_mgc_TP_MSigDb, 0.05))

  save_plot("Ease_mcl_TP_MSigDb_scatter.pdf", Ease_mcl_TP_MSigDb.plot.scatter, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_TP_MSigDb_scatter.pdf", Ease_mgc_TP_MSigDb.plot.scatter, base_width = 5, base_height = 4)
  save_plot("Ease_mcl_TP_MSigDb_overlap_scatter.pdf", Ease_mcl_TP_MSigDb.plot.overlap.scatter, base_width = 5, base_height = 4)
  save_plot("Ease_mgc_TP_MSigDb_overlap_scatter.pdf", Ease_mgc_TP_MSigDb.plot.overlap.scatter, base_width = 5, base_height = 4)
}

if (F) {
  (Ease_mcl_TP.plot.pVal <- makeTPplot(Ease_mcl_TP, minx = 1/3100, maxy = 0.5,
                                  stat = "pVal",
             countbreaksa = log10breaks(0, 80),
             countbreaksb = log10breaks(0, 4), hjust = -0.008))
}
