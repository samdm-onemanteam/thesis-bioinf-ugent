\documentclass[12pt,a4,twoside,openany]{book}

% ==============================================================================
% Preamble
% ==============================================================================

% ------------------------------------------------------------------------------
% Package options
% ------------------------------------------------------------------------------

% helps with writing
\usepackage[textsize=scriptsize,obeyFinal]{todonotes}
\setlength{\marginparwidth}{2.5cm} % fix too narrow todonotes
\usepackage{layout}
\usepackage{lipsum}

% style and appearance
\usepackage{fontspec}
\setmainfont{Calibri}
\setsansfont{Verdana}
\usepackage[labelfont=small,bf]{caption}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usepackage[top=2.5cm, bottom=2.5cm]{geometry}
\usepackage{parskip}
\usepackage{fancyhdr}
\setlength{\headheight}{15pt}
\usepackage{titlesec}
\usepackage{tocbibind}
\usepackage{tocloft}

% hyphenation patterns
\usepackage{polyglossia}
\setdefaultlanguage{english}
\setotherlanguage{dutch}

% other
\usepackage{booktabs} % more table rules
\usepackage{tabu} % more flexible tables
\usepackage{colortbl}
\usepackage{xparse} % required for some macros
\usepackage{amsmath} % more math options
\usepackage[inline]{enumitem} % more control over enumerations
\usepackage{bm} % better bold fonts in math
\usepackage{mathtools} % align symbols in matrices
\usepackage{setspace} % change line spacing mid document
\usepackage{subcaption} % subfigures
\usepackage{placeins} % prevent floats from floating too far
\usepackage[ruled]{algorithm} % floats for algorithms
\usepackage{algorithmic} % pseudocode
\usepackage{metalogo} % fancy LuaTeX logo

% pretty source code
\usepackage{listings}
\lstset{
  language=R,
  basicstyle=\scriptsize\ttfamily,
  commentstyle=\ttfamily\color{gray},
  numbers=left,
  numberstyle=\ttfamily\color{gray}\footnotesize,
  stepnumber=1,
  numbersep=5pt,
  backgroundcolor=\color{white},
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  frame=single,
  tabsize=2,
  captionpos=b,
  breaklines=true,
  breakatwhitespace=false,
  title=\lstname,
  escapeinside={},
  keywordstyle={},
  morekeywords={},
  aboveskip=16pt,
  belowskip=-16pt
}

% cross-references (must be loaded last)
\usepackage[authoryear,round]{natbib}
\bibliographystyle{src/tex/embo}
\usepackage{url}
\usepackage{hyperref}
\hypersetup{pdfborder=0 0 0}
\usepackage{cleveref}
\usepackage[acronym,toc]{glossaries}

% ------------------------------------------------------------------------------
% Some styling options
% ------------------------------------------------------------------------------

% change chapter style, to make it look like parts
\addto\captionsenglish{\renewcommand{\chaptername}{Part}}
\titleformat{\chapter}
{\centering\normalfont\Huge\bfseries}
{{\chaptername} \thechapter: }
{0pt}
{\Huge}

% change chap appearance of toc to look like part
\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}}
\setlength{\cftchapnumwidth}{1.7cm}
\newcommand*{\hnode}[1]{%
    \tikz[remember picture]%
    \node[minimum size=0pt,inner sep=0pt,outer sep=4.4pt](#1){};
}
\renewcommand{\cftchappresnum}{
      \hnode{P1}%
      \tikz[remember picture,overlay] {%
          \draw ++($(P1.north west) + (0,0)$)%
              [line width={17pt},black] --%
                ++($(\textwidth,0) + (1ex,0)$);%
          \draw ++($(P1.north west) + (0,0)$)%
              [line width={15pt},gray!60] --%
                ++($(\textwidth,0) + (1ex,0)$);%
      }%
      Part~%
}
\renewcommand{\cftchapaftersnum}{:}
\setlength{\cftbeforesecskip}{6pt}

% specify the header and footer for the parts
\definecolor{headerGray}{HTML}{909090}
\fancypagestyle{mainheader}{%
    \renewcommand{\headrule}{{\color{headerGray}%
    \hrule width\headwidth height\headrulewidth \vskip-\headrulewidth}}
    \renewcommand{\chaptermark}[1]{\markboth{##1}{}}
    \fancyhf{}
    \fancyhead[LE,RO]{\color{headerGray} \chaptername{} \thechapter: \leftmark}
    \fancyfoot[LE,RO]{\thepage}
}

% specify the header and footer for the backmatter
\fancypagestyle{backheader}{%
    \renewcommand{\headrule}{{\color{headerGray}%
    \hrule width\headwidth height\headrulewidth \vskip-\headrulewidth}}
    \renewcommand{\chaptermark}[1]{\markboth{##1}{}}
    \fancyhf{}
    \fancyhead[LE,RO]{\color{headerGray} \nouppercase\leftmark}
    \fancyfoot[LE,RO]{\thepage}
}

% remove braces from equations labels
\creflabelformat{equation}{~#2#1#3}

% ------------------------------------------------------------------------------
% Custom commands
% ------------------------------------------------------------------------------

% inline todonote with arbitrary complex contents
\newcommand\todoin[2][]{
    \todo[inline, caption={2do}, #1]{
        \begin{minipage}
            {\textwidth-4pt}\normalsize{#2}
        \end{minipage}
    }
}

% use this to have figure/table titles in bold text
\newcommand{\bcaption}[2]{\caption[#1]{\small \textbf{#1.} #2}}

% wraps some text in < ... > to mark it as placeholder content
\newcommand{\placeholder}[1]{\textless{}#1\textgreater{}}

% make a glossary entry and a abbreviation entry in one go
\DeclareDocumentCommand{\newdualentry}{ O{} O{} m m m m } {
  \newglossaryentry{gls-#3}{name={#5},text={#5\glsadd{#3}},
    description={#6},#1
  }
  \makeglossaries
  \newacronym[see={[List of Terms:]{gls-#3}},#2]{#3}{#4}{#5\glsadd{gls-#3}}
}

% use this environment to prevent page breaks, no matter what.
\newenvironment{absolutelynopagebreak}
  {\par\nobreak\vfil\penalty0\vfilneg
   \vtop\bgroup}
  {\par\xdef\tpd{\the\prevdepth}\egroup
   \prevdepth=\tpd}

% ------------------------------------------------------------------------------
% Insert and update glossaries (list of abbreviations and explanations of terms)
% ------------------------------------------------------------------------------

\input{src/tex/input/terms_and_abbreviations.tex}
\makeglossaries

% ------------------------------------------------------------------------------
% Metadata
% ------------------------------------------------------------------------------

\newcommand{\theauthor}{Sam De Meyer}
\newcommand{\thetitle}{
    Assessing the performance of network crosstalk analysis combined with
    clustering
}
\newcommand{\thepromotorAtUG}{Prof. Dr. Klaas Vandepoele}
\newcommand{\thepromotorAtSU}{Prof. Dr. Erik Sonnhammer}
\newcommand{\thescientificsup}{Christoph Ogris}

\author{\theauthor{}}
\title{\thetitle{}}

% ------------------------------------------------------------------------------
% Speed up compiling
% ------------------------------------------------------------------------------

%\includeonly{src/tex/parts/introduction}
%\includeonly{src/tex/parts/aim_of_research_project}
%\includeonly{src/tex/parts/results}
%\includeonly{src/tex/parts/discussion}
%\includeonly{src/tex/parts/materials_and_methods}
%\includeonly{src/tex/supp/gsdata}
%\includeonly{src/tex/supp/cluseffect}
%\includeonly{src/tex/supp/hpa}

% ==============================================================================
% Document
% ==============================================================================

\begin{document}

% disables page numbering for title page and confidentiality statement
\pagenumbering{gobble}

% insert title page (is also cover)
\input{src/tex/input/titlepage.tex}

% a blank page following the title page, no page numbers
\newpage \null \pagestyle{empty} \newpage

%% confidentiality statement
%\input{src/tex/input/confidentiality_statement.tex}

%-------------------------------------------------------------------------------
% Frontmatter: roman numbers start here
%-------------------------------------------------------------------------------

\frontmatter

% page number centered at the bottom
\pagestyle{plain}

% acknowledgements
\input{src/tex/input/acknowledgements.tex}

% table of contents / list tables / list of figues
\renewcommand\contentsname{Table of contents}
\tableofcontents
\clearpage
\listoftables
\clearpage
\listoffigures

% Abbreviations and glossaries
\printacronyms[title=List of Abbreviations]
\printglossary[title=List of Terms]

% Dutch summary
\input{src/tex/input/summary_dutch.tex}

% English summary
\input{src/tex/input/summary_english.tex}

%-------------------------------------------------------------------------------
% Mainmatter: arabic numbering
%-------------------------------------------------------------------------------

\mainmatter

% make header and footer as specified
\pagestyle{fancy}
\pagestyle{mainheader}
\assignpagestyle{\chapter}{mainheader}

% acronyms are fully displayed only on their first use, afterwards only the
% abbreviation is used. This means that, by using an acronym in your list of
% terms, which comes first (before the main content, i.e., the parts), it will
% be fully displayed there. So the acronym will not be fully displayed on first
% usage in the main matter. To fix this, reset the acronym usage counters:
\glsresetall

% include the parts
\include{src/tex/parts/introduction}
\include{src/tex/parts/aim_of_research_project}
\include{src/tex/parts/results}
\include{src/tex/parts/discussion}
\include{src/tex/parts/materials_and_methods}

%-------------------------------------------------------------------------------
% Backmatter: attachments with numbering a.1, a.2, ... , b.1, b.2, ...
%-------------------------------------------------------------------------------

%\backmatter

\pagestyle{fancy}
\pagestyle{backheader}
\assignpagestyle{\chapter}{backheader}

% references
\renewcommand{\bibname}{References}
\bibliography{src/tex/library}

% attachments
\appendix
\phantomsection
\addcontentsline{toc}{part}{Attachments}
\include{src/tex/supp/gsdata}
\include{src/tex/supp/cluseffect}
\include{src/tex/supp/hpa}

\end{document}
