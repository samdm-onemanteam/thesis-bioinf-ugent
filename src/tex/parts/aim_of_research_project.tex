\chapter{Aim of Research Project}
\label{cha:aim}

The aims of the study are to establish a robust benchmarking procedure for
evaluating the performance (sensitivity and specificity) of overlap-based
pathway analysis tools and the newer network-based crosstalk analysis tool
\gls{BinoX}. Once this benchmarking procedure is established, I will use this
to assess the effect of clustering on the performance of enrichment tools. If
clustered modules are more ``pure" (they contain more genes from the same
pathway/biological process/etc.), then sensitivity---and, possibly,
\gls{FPR}---might be increased by running enrichment tools on separate modules
instead of the entire list at once. The best performing tools, with or without
clustering, will then be used to enrich pathways in tissue specific gene lists
from the \acrlong{HPA} \citep{Uhlen2010,Uhlen2015}.

\section{Benchmarking pathway analysis methods}
\label{sec:aim_benchmarking_pathway_analysis_methods}

The problems and possible solutions for benchmarking pathway analysis methods
have already been introduced in \cref{sub:how_to_benchmark_pathway_analysis}.
In this thesis, I will use an extension of the third approach which was
introduced by \cite{Tarca2012}. This benchmark is based on real data, which is
preferable over simulated data because one cannot be certain that findings
based on simulated data will also be true in real use case scenarios. A part of
the workflow of this benchmark will have to be changed however to meet the
needs of \gls{BinoX} and to guarantee certain quality criteria; this is
explored in detail in \cref{sec:benchmarking_pathway_analysis_methods}. In
addition to the overlap based tools and \gls{BinoX}, I will also include the
\gls{PADOG} tool, made by the same authors as the benchmark. This will allow
comparison of results obtained by the benchmark presented here with earlier
results \citep{Tarca2012,Dong2016}.

\section{Assessing the effect of clustering}
\label{sec:aim_assessing_the_effect_of_clustering}

We assume that gene sets derived from experiments are noisy and comprise
multiple pathways and/or biological processes. This can make it more difficult
for pathway analysis methods to detect relevant pathways. Clustering the genes
in the gene set prior to running the tools can be used to counter this: instead
of using the entire gene set, the tools will be used for every module
separately. The gene set can be clustered based on a \gls{FA} network. Any
network based clustering tool can be used for this, examples are MCode
\citep{Bader2003}, FastCommunity \citep{Clauset2004}, \gls{MCL}
\citep{Dongen2000} and \gls{MGclus} \citep{Frings2013}, of which the latter two
will be in used in this thesis. The main motivation for doing this is that
genes in one module are more likely part of the same biological process or
pathway. In addition noisy genes might be removed by clustering into single
gene modules (\cref{fig:subclus}). Although this strategy might improve
sensitivity, it might also increase the false positive rate. I will evaluate
the trade-off between sensitivity and false positive rate to determine whether
clustering results in an improvement or not.

\section{Analysing the \acrlong{HPA}}
\label{sec:analysing_the_hpa}

Running the overlap based tools and \gls{BinoX}---with and without
clustering---on the \gls{HPA} might give more insight into each tool. Is it
necessary to run several tools or does one method find everything that the
other methods find combined? Do several tools find the same aspects of the
underlying biology or are they complementary? Are there important pathways that
are missed by the tools without clustering, but can be found with clustering?

\begin{figure}[]
    \centering
    \includegraphics[width=0.9\linewidth]{./src/fig/subcluster}
    \bcaption{Combining pathway analysis with clustering}{
        The traditional form of pathway analysis is to take a gene set in its
        entirety and enrich that to pathways from an annotation database (1).
        Grey genes represent ``noise": genes that ended up in the gene set due
        to technical or biological variation but are not actually \gls{DE}.
        Dark green and red genes are genes that belong to two different
        pathways that have an important role in the condition/experiment that
        led to the input gene set.  The traditional method will, for most
        tools, give one p-value per pathway (2).
        %
        An alternative could be to first extract the subgraph comprising the
        nodes of the input gene set from a \gls{FA} network (3) \& (4). By
        extracting the subgraph, some genes will become completely disconnected
        or might not have been in the \gls{FA} network to begin with.  These
        genes are then removed, thus extracting a subgraph can lead to a loss
        of information (black crosses).  Then, the subgraph is clustered (5)
        and in the ideal scenario, different pathways would cluster together in
        the same module. Poorly connected genes, primarily ``noise" genes in
        the ideal case, might end up in single-node modules and are removed
        before doing further analysis (white crosses). Finally, all modules are
        enriched separately to pathways in an annotation database (6) and the
        results are combined (7).
        %
        Both the clustered and unclustered analysis might be combined into a meta
        analysis to get a complete picture of the underlying biological
        processes.
    }
    \label{fig:subclus}
\end{figure}
