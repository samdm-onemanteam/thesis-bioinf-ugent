\chapter{Materials and Methods}
\label{cha:materials_and_methods}

\section{Moderated t-test}
\label{sec:moderated_t_test}

All microarray studies in the benchmark
(\cref{sec:benchmarking_pathway_analysis_methods}) compare samples associated
with a certain disease to healthy samples, they fall under two categories in
terms of experimental design:
\begin{enumerate}
    \item Either $a$ disease tissue samples versus $b$ healthy tissue samples,
        not paired, or
    \item $a$ disease tissue samples versus $a$ paired healthy tissue samples,
        each pair coming from one patient.
\end{enumerate}
From hereon, I will refer to the first and second design as the ``unpaired" and
``paired" design respectively.

The moderated t-test was used to assess which genes are \gls{DE} between the
disease and healthy phenotypes \citep{Smyth2004}. Here, the moderated t-values
are derived from the coefficients of a linear model that is fitted for each
gene.
\begin{equation}
    \bm{y}_g  = \bm{X} \bm{\alpha} + \bm{\epsilon}
\end{equation}
Where the column vector $\bm{y}_g$ contains the normalized and summarized
expression values of the microarray probes of one gene for all samples.
$\bm{X}$ is the design matrix of the linear model and $\bm{\alpha}$ is a column
vector containing the coefficients to be estimated.

\paragraph{Model specification}
\label{par:model_specification}
For the unpaired designs, consider a response vector $\bm{y}_g$ of length $a+b$
where the first $a$ entries are expression values from diseased tissues, and
the last $b$ entries are expression values from healthy tissues. Then, the
following design matrix was used:
\begin{equation}
    \bm{X} =
    \begin{bmatrix}
        1 & 0 \\
        \vdots & \vdots \\
        0 & 1 \\
    \end{bmatrix}
\end{equation}
Where the first $a$ rows are $[1,0]$ and the last $b$ rows are $[0,1]$. By
minimizing the residual sum of squares for this model, the estimate of the
first coefficient $\hat\alpha_{g1}$ will simply be the average of the disease
expression values, and the estimate of the second coefficient $\hat\alpha_{g2}$
will be the average of the healthy expression values.

For the paired designs, consider a response vector $\bm{y}_g$ of length $2a$,
where every entry from $y_{gi}, i \in [1, \dots, a]$ is paired with an entry
$y_{g(i+a)}$. Then, the following design matrix was used:
\begin{equation}
    \bm{X} =
    \begin{bmatrix}
        1 & 0 & 0 & 0 & \dots \\
        1 & 0 & 1 & 0 & \dots \\
        1 & 0 & 0 & 1 & \dots \\
        \vdots & \vdots & \vdots & \vdots & \ddots \\
        0 & 1 & 0 & 0 & \dots \\
        0 & 1 & 1 & 0 & \dots \\
        0 & 1 & 0 & 1 & \dots \\
        \vdots & \vdots & \vdots & \vdots & \ddots \\
    \end{bmatrix}
\end{equation}
Where the first $a$ rows correspond to disease samples, and the last $a$ rows
correspond to healthy samples. In this model, the first coefficient
$\alpha_{g1}$ represents the expression value for gene $g$ in diseased tissue
of patient $1$ (an arbitrarily chosen patient). Similarly, the second
coefficient $\alpha_{g2}$ represents the expression value of gene $g$ in
healthy tissue of patient $1$. All other coefficients $\alpha_{g(i+1)}, i \in
[2, \dots, a]$, represent the difference in expression of gene $g$ for patient
$i$ versus patient $1$.

\paragraph{Contrast specification}
\label{par:contrast_specification}
After fitting the model, the following contrast matrix, a vector in this
case,\footnote{
    Using the notation of \cite{Smyth2004}, the contrast matrix would be
    written as $\bm{C}$, but since there is only one contrast of interest here,
    $\bm{C}$ reduces to a vector, which I will denote as $\bm{c}$.
} is specified:
\begin{equation}
    \begin{matrix}
    \text{unpaired} \\
    \text{designs}
    \end{matrix}\ :
    \quad
    \bm{c} =
    \begin{bmatrix*}[r]
        1 \\
        -1
    \end{bmatrix*}
    \qquad
    \qquad
    \begin{matrix}
    \text{paired} \\
    \text{designs}
    \end{matrix}\ :
    \quad
    \bm{c} =
    \begin{bmatrix*}[r]
         1 \\
        -1 \\
         0 \\
         0 \\
        \vdots
    \end{bmatrix*}
\end{equation}
The contrast of interest\footnote{
    In \cite{Smyth2004}, the contrasts of interest are denoted as the vector
    $\bm{\beta}_g$, and an individual contrast $j$ as $\beta_{gj}$. Since
    there is only one contrast of interest here, I am dropping the subscript
    $j$.
}, $\beta_g$, which is the difference in expression of a gene between the
disease and healthy phenotype, is then obtained by:
\begin{equation}
    \beta_g = \bm{c}^\top \bm{\alpha}_g
\end{equation}
For unpaired designs, $\hat\beta_g$ is simply the difference of the mean for
the disease and healthy samples. For paired designs, $\hat\beta_g =
\hat\alpha_{g1} - \hat\alpha_{g2}$, which is the estimated difference in
expression excluding patient effects.

\paragraph{Moderated t-value}
\label{par:moderated_t_value}
The ordinary t-value for the contrast $\beta_g$ would be obtained by:
\begin{equation}
    t_g = \frac{\hat\beta_g}{s_g\sqrt{v_g}}
    \label{eq:lmttest}
\end{equation}
Where $s_g$ is the estimated standard deviation of $\bm{y}_g$ and $v_g$ is the
unscaled variance of $\beta_g$. I.e.: $v_g = \bm{c}^\top \bm{V}_g \bm{c}$ where
$\bm{V}_g$ is the unscaled covariance matrix of $\bm{\alpha}$. This means that
for an ordinary t-statistic, the variance of each gene is estimated
independently.  \cite{Smyth2004} proposed a moderated t-statistic where the
variance of a gene is weighted by the ordinary variance and a prior variance
derived from all genes:
\begin{equation}
    \tilde s^2_g = \frac{d_0 s^2_0 + d_g s^2_g}{d_0 + d_g}
\end{equation}
Where $s^2_0$ and $d_0$ are the prior variance and degrees of freedom
respectively, which are estimated from the data (see \cite{Smyth2004} for
derivation of these values). Using $\tilde s_g$ instead of $s_g$ in
\cref{eq:lmttest} and expanding $v_g = \bm{c}^\top \bm{V}_g \bm{c} =
\bm{c}^\top (\bm{X}^\top \bm{X})^{-1} \bm{c}$ yields, in this case, the
following equation for the moderated t-value:
\begin{equation}
    \tilde t_g = \frac{\hat\alpha_{g1} - \hat\alpha_{g2}}
                      {\tilde s_g\sqrt{\frac{1}{a} + \frac{1}{b}}}
\end{equation}
Where, for paired designs, $b = a$ and, as mentioned earlier, for unpaired
designs, $\hat\alpha_{g1}$ and $\hat\alpha_{g2}$ are simply the sample means.
Thus, the variance of the contrast will decrease proportional to the sample
sizes and to how well balanced the sample sizes are ($a/b \approx 1$).

The variance of $\bm{y}_g$ is easily estimated too small by accident in
microarray studies with low sample sizes, inflating the corresponding p-value.
The opposite, overestimation of the variance, can also easily happen for the
same reason. Using the moderated t-test, sample variances are shrunken towards
a common value, reducing the effect of variance over- or underestimation.
Therefore the moderated t-test is advantageous over the ordinary t-value.

\paragraph{Final notes}
\label{par:final_notes}
Using the procedure described above, a moderated t-value is obtained for each
gene. Positive t-values indicate that a gene has higher expression in diseased
tissues, while negative t-values indicate the opposite.

P-values obtained from these t-statistics were adjusted for multiple testing
using the \gls{BH} procedure \citep{Benjamini1995}.

All steps described in this section were carried out in \verb|R|
\citep{RDevelopmentCoreTeam2014} using the package \verb|limma|
\citep{Ritchie2015}. See the Implementation of the \verb|makeModerated|
\verb|TTestTopTable| function in the \verb|BinoX| package for the code.

\section{Translating gene identifiers}
\label{sec:translating_gene_identifiers}

Two methods have been used to map probeset identifiers to Ensembl IDs. The
first method, used in
\cref{sub:mapping_entrez_ids_to_ensembl_through_the_funcoup_network} was used
for the benchmark based on the microarray data. The second, used in
\cref{sub:entrez_to_ensembl_mapping_from_earlier_publication} was used for the
\gls{MSigDB} based benchmark.

\subsection{Mapping IDs to Ensembl through the FunCoup network}
\label{sub:mapping_entrez_ids_to_ensembl_through_the_funcoup_network}

All microarray datasets in the benchmark are either based on the Affymetrix
\verb|hgu133a| chip or the \verb|hgu133plus2| chip. Both chips have an
annotation package in Bioconductor named \verb|hgu133a.db| and
\verb|hgu133plus2.db| respectively. For both packages version 3.2.2 was used.
These packages provide direct probeset to Ensembl translations, but there are
often many to many relationships between probeset and gene identifier.  This
problem was dealt with in two steps: First, from all Ensembl IDs, the one with
highest node degree in FunCoup was taken. Second, if multiple probesets---each
one having an associated test statistic for differential expression--map to the
same Ensembl ID, the one with lowest p-value was taken. In pseudocode:

\begin{algorithm}[h!]
\footnotesize{
\begin{algorithmic}
    \REQUIRE probeset IDs mapped to a test statistic (and p-value) for differential expression
    \ENSURE Ensembl IDs mapped to a test statistic (and p-value) for differential expression
    \STATE \STATE
    \COMMENT {Step 1}
    \STATE \emph{mapping} $\gets$ empty dictionary
    \FORALL {\emph{probeset ID} from platform}
    \STATE \emph{matching Ensembl IDs} $\gets$ all Ensembl IDs from annotation package matching \emph{probeset ID}
        \IF {there are matching Ensembl IDs}
            \IF {any matching Ensembl IDs $\in$ FunCoup network}
                \STATE \emph{mapping}(\emph{probeset ID}) $\gets$ highest degree node from \emph{matching Ensembl IDs}
            \ELSE
                \STATE \emph{mapping}(\emph{probeset ID}) $\gets$ first ID from \emph{matching Ensembl IDs}
            \ENDIF
        \ENDIF
    \ENDFOR

    \STATE \STATE
    \COMMENT {Step 2}
    \STATE \emph{EnsemblID\_to\_testStatistic} $\gets$ empty dictionary
    \FORALL {\emph{Ensembl ID} in values of \emph{mapping}}
        \STATE \emph{matching probeset IDs} $\gets$ all keys (probeset IDs) that map to the current \emph{Ensembl ID} in \emph{mapping}
        \STATE \emph{testStat} $\gets$ most extreme test statistic for all probesets in \emph{matching probeset IDs}
        \STATE \emph{pValue} $\gets$ p-value associated with \emph{TestStat}
        \STATE \emph{EnsemblID\_to\_testStatistic}(\emph{Ensembl ID}) $\gets$ (\emph{testStat}; \emph{pValue})
    \ENDFOR
\end{algorithmic}
}
\end{algorithm}

In the end, a non-redundant list of Ensembl IDs is obtained, each one having an
associated test statistic and p-value for differential expression. The
translated list will be shorter than the number of probeset IDs since some
probeset IDs cannot be translated to Ensembl IDs and because many to many
mappings are resolved.

For translating \gls{KEGG} IDs to Ensembl, the same procedure as ``Step 1" was
used, except that Entrez IDs were translated instead of probeset IDs.  For
mapping Entrez to Ensembl, the \verb|org.Hs.eg.db| package version 3.2.3 from
Bioconductor was used. Duplicate Ensembl IDs (arising from multiple Ensembl to
one Entrez ID mappings) were removed to obtain non redundant gene sets.

\subsection{Entrez to Ensembl mapping from earlier publication}
\label{sub:entrez_to_ensembl_mapping_from_earlier_publication}

The CrossTalkZ pathway analysis tool \citep{McCormack2013}, also requiring
Ensembl IDs, has been tested earlier with simulated data as well as with
\gls{KEGG} and \gls{MSigDB} gene sets. For the benchmark based on \gls{MSigDB}
data in this thesis (\cref{sub:msigdb}), I have reused these already translated gene
sets.

\section{False positive rate estimation}
\label{sec:false_positive_rate_estimation}

\subsection{Sample label swap}
\label{sub:sample_label_swap}

For testing the \gls{FPR}, microarray data was taken but the sample labels were
permuted $n$ times. This way, every sample is randomly assigned a disease or
healthy label. Because the groups are now random, differential expression
should not be observed after correcting for multiple testing. This is true in
all but a few cases were the permuted sample labels closely match the original
sample labels. Using this sample label swap, genes that are correlated are more
likely to be selected together. Therefore, this method can be useful for
testing whether tools account for gene-gene correlations.

\subsection{Gene set permutation}
\label{sub:gene_set_permutation}

For the alternative \gls{FPR} test, the \glspl{qgs} were permuted by replacing
every gene by a new one with similar node degree (maximum $5\%$ difference).
Replacement genes were not allowed to be in the \gls{qgs} set and may not have
been picked before. When there are no such genes, a completely random gene from
FunCoup is picked as replacement.

\begin{algorithm}[h!]
\footnotesize{
\begin{algorithmic}
    \REQUIRE input gene set; \gls{FA} network
    \ENSURE permuted gene set with approximately the same node degree distribution as input gene set

    \STATE
    \STATE \emph{permuted gene set} $\gets$ empty set
    \FORALL {\emph{gene} $\in$ \emph{input gene set}}
        \STATE \emph{candidates} $\gets$ genes differing no more than $5\%$ in node degree from \emph{gene} given \gls{FA} network
        \STATE \emph{candidates} $\gets$ all \emph{candidates} $\notin$ \emph{input gene set}
        \STATE \emph{candidates} $\gets$ all \emph{candidates} $\notin$ \emph{permuted gene set}
        \IF {there are candidates}
            \STATE add random gene from \emph{candidates} to \emph{permuted gene set}
        \ELSE
            \STATE \emph{unpicked} $\gets$ genes from \gls{FA} network $\notin$ \emph{input gene set}
            \STATE \emph{unpicked} $\gets$ all \emph{unpicked} $\notin$ \emph{permuted gene set}
            \STATE add random gene from \emph{unpicked} to \emph{permuted gene set}
        \ENDIF
    \ENDFOR
\end{algorithmic}
}
\end{algorithm}

\section{Procedures used for benchmarking}
\label{sec:procedures_used_for_benchmarking}

\subsection{Benchmark based on microarray data}
\label{sub:benchmarking_based_on_microarray_data}

\subsubsection{Sensitivity and prioritisation test}
\label{ssub:sensitivity_and_prioritisation_test}

\begin{enumerate}
    \item The 26 datasets from \cref{tab:gsd} were downloaded
        from NCBI \gls{GEO}.
    \item The moderated t-test was done as described in
        \cref{sec:moderated_t_test} using the samples given in
        \cref{sec:value_distributions_of_gold_standard_microarray_data}.
    \item Probeset identifiers were translated to Ensembl as described in
        \cref{sub:mapping_entrez_ids_to_ensembl_through_the_funcoup_network}.
        \begin{description}
            \item [PADOG] was run by passing the following input to the
                \verb|PADOG| function from the \verb|PADOG| Bioconductor
                package
                \url{http://www.bioconductor.org/packages/release/bioc/html/PADOG.html}.:
                \begin{enumerate}
                    \item The probeset to Ensembl translations from step 3
                    \item The microarray data from step 1
                    \item The \gls{KEGG} pathways translated to Ensembl as
                        described in
                        \cref{sub:mapping_entrez_ids_to_ensembl_through_the_funcoup_network}
                    \item Number of iterations = 50
                \end{enumerate}
        \end{description}
    \item For each dataset, all genes with a \gls{BH} adjusted p-value of at
        least $0.01$ and a fold change of at least $50\%$ were used to form a gene
        set. This means both up and downregulated genes are included in the gene
        set.
        \begin{description}
            \item [BinoX] was run on these gene sets with
                \verb|relationType = +|, i.e. only enrichment p-values were
                calculated. The FunCoup network version 3.0 was used with and
                edge weight cutoff of $0.8$ and $150$ randomisations.
                \gls{KEGG} pathways were the same as for \gls{PADOG}.
            \item [EASE \& Fisher] methods were run on the same gene sets and
                \gls{KEGG} pathways as \gls{BinoX}.
        \end{description}
    \item Gene sets were clustered by:
        \begin{enumerate}
            \item Extracting a subnetwork from the complete FunCoup graph (no
                cutoff) containing the genes from the gene set.
            \item Removing genes that have no edges in the subnetwork.
            \item Applying either \gls{MGclus} or \gls{MCL} to the obtained
                subnetwork. Both tools were run with default settings: merge
                gain cutoff of $0$ for \gls{MGclus} and inflation of $2$ for
                \gls{MCL}. Edge weights were used for both algorithms.
            \item Removing all genes that end up in a one-node module.
        \end{enumerate}
        \begin{description}
            \item [BinoX] was run on each module of each gene set versus all
                \gls{KEGG} pathways. The minimum allowed gene set size was
                lowered to $2$. P-value correction was done across all module
                to pathway combinations tested. Per pathway, the lowest
                p-value across all modules was kept.
            \item [EASE \& Fisher] were also run on all modules versus all
                \gls{KEGG} pathways, and also corrected for all combinations.
                Per pathway, the lowest p-value across all modules was kept.
        \end{description}
    \item Sensitivity and prioritisation was done on the output from
        \gls{PADOG} in step 3 and the output from \gls{BinoX}, EASE and Fisher
        from step 4 and 5.
        \begin{description}
            \item[sensitivity] This is the number of \glspl{tpw} that have
                a p-value below the significance cutoff. The sensitivity over a
                range of cutoff values is given in
                \cref{fig:tarca_inf_TP_p,fig:tarca_600_clus_TP_p}.
            \item[prioritisation] The pathways are sorted on p-value per
                gene set, then their position in the sorted list is determined.
                This is divided by the total number of pathways and
                multiplied by $100$ to get the rank percentage. Sometimes a
                p-value cannot be determined or is equal to exactly $1$.  In
                this case, the rank percentage was set to $100\%$ for the
                violin plots in
                \cref{fig:tarca_inf_clus_TP_r,fig:tarca_600_clus_TP_r}. For the
                boxplots these rank percentages were omitted.
        \end{description}
\end{enumerate}

\subsubsection{Specificity test}
\label{ssub:specificity_test}

Two tests have been used. For the first, the sample labels from the microarrays
were permuted (see \cref{sub:sample_label_swap}). Then step 2 and step 3 of the
sensitivity benchmark is repeated on this false data. Instead of step 4, the
first $n$ genes are picked (sorted on p-value for \gls{DE}) where $n$ is a
number drawn randomly from the gene set sizes of the true gene sets obtained in
step 4.  Then, step 5 is again the same as in the sensitivity test. The
specificity is simply the proportion of p-values that fall below a certain
cutoff. The specificity over a range of cutoffs is shown in
\cref{fig:tarca_inf_clus_FP_ls,fig:tarca_600_clus_FP_ls}. For the second: the
true gene sets from step 4 of the sensitivity test were permuted as explained
in \cref{sub:gene_set_permutation}. These gene sets where then clustered as
described in step 5 of the sensitivity test. Specificity is again the
proportion of p-values below a certain cutoff
(\cref{fig:tarca_inf_clus_FP_gp,fig:tarca_600_clus_FP_gp}).

Both of the specificity tests have been repeated $10$ times on all 26
datasets/gene sets.

\subsection{Benchmark based on \acrshort{MSigDB} gene sets}
\label{sub:procedure_for_benchmarking_based_on_msigdb_gene_sets}

\subsubsection{Sensitivity and prioritisation test}
\label{ssub:sensitivity_and_prioritisation_test_msigdb}

\gls{KEGG} and \gls{MSigDB} gene sets were already made available in Ensembl
IDs by \cite{McCormack2013}. The \gls{MSigDB} gene sets from
\cref{tab:gsd_MSigDB} were enriched versus all \gls{KEGG} pathways.  They were
also clustered as described in step 5 of
\cref{sub:benchmarking_based_on_microarray_data} and then every module was
enriched to every \gls{KEGG} pathway. Sensitivity and prioritisation were
calculated as described in step 6 of
\cref{sub:benchmarking_based_on_microarray_data}.

\subsubsection{Specificity test}
\label{ssub:specificity_test_msigdb}

Since no microarray data is available, sample label swaps have not been used
for specificity testing. Specificity testing based on gene set permutations was
done in exactly the same way as described in
\cref{sub:benchmarking_based_on_microarray_data}: by permuting the \gls{MSigDB}
gene sets while retaining first order node degree. Once again, every gene set
was permuted 10 times.

\section{Hardware and software used}
\label{sec:computer_platforms_used}

\subsubsection{hardware}
\label{ssub:hardware}

Two platforms have been used for both building the \verb|BinoX| package
and running the benchmarks:

\begin{table}[h]
    \centering
    \begin{tabu}{X[16,l,m] X[16,l,m] X[4,r,m] X[15,r,m] X[12,r,m]}
        \toprule
        OS & kernel release & cores & max speed (MHz) & memory (kb) \\
        \midrule
        Arch linux & 4.5.1-1-ARCH & 4 & $3000.000$ & $8046156$ \\
        Ubuntu 14.04.4 LTS & 3.13.0-79-generic & 8 & $800.000$ & $33013632$ \\
        \bottomrule
    \end{tabu}
    \bcaption{Computer platforms used}{
        Arch linux has a rolling release model, no version numbers are used.
    }
    \label{tab:hardware}
\end{table}

\subsubsection{Software for data analysis}
\label{ssub:software_for_data_analysis}

The \verb|R| version and packages used are listed below:

{\footnotesize
\begin{verbatim}
R version 3.3.0 (2016-05-03)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 14.04.4 LTS

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C
 [3] LC_TIME=sv_SE.UTF-8        LC_COLLATE=en_US.UTF-8
 [5] LC_MONETARY=sv_SE.UTF-8    LC_MESSAGES=en_US.UTF-8
 [7] LC_PAPER=sv_SE.UTF-8       LC_NAME=C
 [9] LC_ADDRESS=C               LC_TELEPHONE=C
[11] LC_MEASUREMENT=sv_SE.UTF-8 LC_IDENTIFICATION=C

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base

other attached packages:
[1] BinoX_0.0.1    nvimcom_0.9-14

loaded via a namespace (and not attached):
 [1] igraph_1.0.1         Rcpp_0.12.3          AnnotationDbi_1.32.3
 [4] magrittr_1.5         roxygen2_5.0.1       BiocGenerics_0.16.1
 [7] devtools_1.10.0      IRanges_2.4.8        munsell_0.4.3
[10] doParallel_1.0.10    colorspace_1.2-6     foreach_1.4.3
[13] plyr_1.8.3           stringr_1.0.0        tools_3.3.0
[16] parallel_3.3.0       grid_3.3.0           Biobase_2.30.0
[19] gtable_0.2.0         DBI_0.3.1            withr_1.0.1
[22] iterators_1.0.8      digest_0.6.9         readr_0.2.2
[25] ggplot2_2.1.0        S4Vectors_0.8.11     codetools_0.2-14
[28] memoise_1.0.0        RSQLite_1.0.0        limma_3.26.8
[31] stringi_1.0-1        scales_0.4.0         stats4_3.3.0
\end{verbatim}
}

\subsubsection{Software used for making this document}
\label{ssub:software_used_for_making_this_document}

This document was typeset by the author using \LuaTeX~
(\url{http://www.luatex.org/}), images were created with:
inkscape    (\url{https://inkscape.org/en/}),
TikZ        (\url{https://www.ctan.org/pkg/pgf}),
ggplot2     (\url{http://ggplot2.org/}),
VennDiagram (\url{https://cran.r-project.org/web/packages/VennDiagram/index.html}) and
Cytoscape   (\url{http://www.cytoscape.org/}).
