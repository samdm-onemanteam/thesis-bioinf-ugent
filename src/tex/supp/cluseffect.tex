\chapter{Additional results on clustering combined with pathway analysis}
\label{cha:additional_results_on_clustering_combined_with_pathway_analysis}

There are many parameters that can be changed in the benchmark presented in
this thesis. There are two clustering methods and two ways of estimating the
\gls{FPR} for example. There is also the alternative gold standard data based
on \gls{MSigDB} gene sets. Finally there are different tools that can be used
in combination with clustering. This leads to dozens of possible ways to run
the benchmark. To keep the main text to the point, not all combinations I
examined are included there. Here I present a few other results from the
benchmark using different settings.

\clearpage
% -----------------------------------------------------------------------------
\section{First benchmark: microarray data}
\label{sec:first_benchmark_microarray_data}
% -----------------------------------------------------------------------------

\begin{figure}[bh]
    \centering
    \begin{subfigure}[c]{\textwidth}
        \includegraphics[height=6.0cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_TP_r}
        \caption{}
        \label{fig:tarca_600_clus_TP_r}
    \end{subfigure}

    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[width=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_TP_p}
        \caption{}
        \label{fig:tarca_600_clus_TP_p}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_FP_ls}
        \caption{}
        \label{fig:tarca_600_clus_FP_ls}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_FP_gp}
        \caption{}
        \label{fig:tarca_600_clus_FP_gp}
    \end{subfigure}
    \bcaption{
        Performance of pathway analysis combined with clustering on smaller
        gene sets%
    }{
        The method used for extracting a gene set from microarray data has a
        huge impact on pathway analysis methods that take a gene set as input.
        This effect is illustrated here by extracting gene sets using the same
        cutoffs as in \cref{fig:tarca_inf_clus} but limiting the maximum gene
        set size to 600, which is a little over the largest \gls{KEGG} pathway
        size (olfactory transduction,
        \href{http://www.genome.jp/dbget-bin/www_bget?pathway+hsa04740}{hsa04740}).
        Shown are (\textbf{a}) the rank percentages of the \glspl{tpw},
        (\textbf{b}) the true positive rates and (\textbf{c}), (\textbf{d}),
        the false positive rates using label swap and gene permutation
        respectively. Additional figures using q-values are given in
        \cref{fig:tarca_600_clus_q}.
    }
    \label{fig:tarca_600_clus}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Ease_mcl_FP}
        \caption{\gls{MCL}, label swap}
        \label{fig:Ease_mcl_FP_labelswap}
    \end{subfigure}%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Ease_mgc_FP}
        \caption{\gls{MGclus}, label swap}
        \label{fig:Ease_mgc_FP_labelswap}
    \end{subfigure}

    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Ease_mcl_FP_geneperm}
        \caption{\gls{MCL}, gene set permutation}
        \label{fig:Ease_mcl_FP_geneperm}
    \end{subfigure}%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Ease_mgc_FP_geneperm}
        \caption{\gls{MGclus}, gene set permutation}
        \label{fig:Ease_mgc_FP_geneperm}
    \end{subfigure}
    \bcaption{Distribution of false positives for the EASE tool}{
        Randomized modules were generated as explained in \cref{fig:BinoX_FP}
        and all module to \gls{KEGG} pathway combinations were analysed using
        the EASE score. Shown here are the \gls{BH} adjusted p-values for
        enrichment that are below 0.20. Axes and colors are as in
        \cref{fig:BinoX_FP}.
    }
    \label{fig:Ease_FP}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Binox_mgc_TP}
        \caption{}
        \label{fig:Binox_mgc_TP}
    \end{subfigure}~~~~%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Binox_mgc_TP_nodegree}
        \caption{}
        \label{fig:Binox_mgc_TP_nodegree}
    \end{subfigure}
    \bcaption{
        Distribution of true positives for the \gls{BinoX} tool using \gls{MGclus}%
    }{
        In the main text, I only show this distribution for \gls{BinoX}
        combined with \gls{MCL} and argue that module size and average node
        degree are not informative features that set \glspl{tpw} apart from
        other pathways. The same seems to be true when using \gls{MGclus}
        instead of \gls{MCL}. Axes and colors in (\textbf{a}) and (\textbf{b})
        are as in \cref{fig:BinoX_TP}.
    }
    \label{fig:BinoX_TP_mgc}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=0.9\textwidth]{src/fig/Clutsering_dists/Ease_mgc_TP_scatter}
        \caption{}
        \label{fig:Ease_mgc_TP_overlap}
    \end{subfigure}~~~~%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=0.9\textwidth]{src/fig/Clutsering_dists/Ease_mgc_TP_overlap_scatter}
        \caption{}
        \label{fig:Ease_mgc_TP_overlap_scatter}
    \end{subfigure}
    \bcaption{
        Distribution of true positives for the Ease tool using \gls{MGclus}%
    }{
        The analysis from \cref{fig:Ease_TP} was repeated but using
        \gls{MGclus} instead. Due to sparsity of the data, two dimensional
        binning is not very well suited to visualise the data. The data is now
        displayed as a scatterplot instead, but the meaning of the axes have
        not changed from \cref{fig:Ease_TP}. Big red dots represent a module to
        \gls{tpw} combination, all other dots are module to non \gls{tpw}
        combinations.
    }
    \label{fig:Ease_TP_mgc}

\end{figure}

\FloatBarrier
\clearpage
% -----------------------------------------------------------------------------
\section{Second benchmark: \acrshort{MSigDB} data}
\label{sec:second_benchmark_msigdb_data}
% -----------------------------------------------------------------------------

\begin{figure}[bh]
    \centering
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Binox_mgc_TP_MSigDb}
        \caption{}
        \label{fig:Binox_mgc_TP_MSigDb}
    \end{subfigure}%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=\textwidth]{src/fig/Clutsering_dists/Binox_mgc_TP_MSigDb_nodegree}
        \caption{}
        \label{fig:Binox_mgc_TP_MSigDb_nodegree}
    \end{subfigure}

    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=0.9\textwidth]{src/fig/Clutsering_dists/Ease_mgc_TP_MSigDb_scatter}
        \caption{}
        \label{fig:Ease_mgc_TP_MSigDb_scatter}
    \end{subfigure}%
    \begin{subfigure}[b]{0.40\pagewidth}
        \includegraphics[width=0.9\textwidth]{src/fig/Clutsering_dists/Ease_mgc_TP_MSigDb_overlap_scatter}
        \caption{}
        \label{fig:Ease_mgc_TP_MSigDb_overlap_scatter}
    \end{subfigure}
    \bcaption{
        Distribution of true positives for \acrshort{MSigDB} gene sets using
        \gls{MGclus}%
    }{
        The \gls{MSigDB} data from \cref{tab:gsd_MSigDB} that was analysed in
        \cref{fig:MSigDB_TP} is here reanalysed using \gls{MGclus} instead of
        \gls{MCL}. Axes and colors are again as in \cref{fig:MSigDB_TP}.
        \gls{BinoX} output is shown in (\textbf{a}) and (\textbf{b}) and EASE
        output is shown in (\textbf{c}) and (\textbf{d}).
    }
    \label{fig:MSigDB_TP_mgc}
\end{figure}

\FloatBarrier
\clearpage
% -----------------------------------------------------------------------------
\section{Sensitivity and specificity tests using q-values}
\label{sec:sensitivity_and_specificity_tests_using_q-values}
% -----------------------------------------------------------------------------

In the main text, sensitivity and specificity plots have been given using
p-values instead of q-values (= p-values controlled with \gls{BH} procedure).
The reason for this is that for some tools there are many ties created at
either $0$ or $1$ by adjusting the p-values. These ties make it more difficult
to distinguish the lines on the plots. But it is common practice to control the
p-value for multiple testing. For completeness, here are the same plots
repeated but the sensitivity/specificity is given over a range of q-values
instead of p-values. The results are always corrected across all pathways and
datasets at once. When clustering is used, the correction is done across all
pathways and modules at once. Every figure below is linked to an equivalent
figure somewhere else in the text that uses p-values. Follow these links for
the figure description.

\begin{figure}[bh!]
    \centering
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[width=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_TP_q}
        \caption{}
        \label{fig:tarca_inf_TP_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_FP_ls_q}
        \caption{}
        \label{fig:tarca_inf_FP_ls_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_FP_gp_q}
        \caption{}
        \label{fig:tarca_inf_FP_gp_q}
    \end{subfigure}
    \bcaption{Comparing \gls{BinoX} with earlier tools (q-values)}{
        The same data as presented in \cref{fig:tarca_inf} is presented here
        but using q-values as a significance cutoff instead of p-values.
    }
    \label{fig:tarca_inf_q}
\end{figure}

\begin{figure}[bh!]
    \centering
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[width=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_clus_TP_q}
        \caption{}
        \label{fig:tarca_inf_clus_TP_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_clus_FP_ls_q}
        \caption{}
        \label{fig:tarca_inf_clus_FP_ls_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_inf_clus_FP_gp_q}
        \caption{}
        \label{fig:tarca_inf_clus_FP_gp_q}
    \end{subfigure}
    \bcaption{Assessing the effect of clustering (q-values)}{
        The same data as presented in \cref{fig:tarca_inf_clus} is presented
        here but using q-values as a significance cutoff instead of p-values.
    }
    \label{fig:tarca_inf_clus_q}
\end{figure}

\begin{figure}[bh!]
    \centering
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[width=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_TP_q}
        \caption{}
        \label{fig:tarca_600_clus_TP_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_FP_ls_q}
        \caption{}
        \label{fig:tarca_600_clus_FP_ls_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-Tarca-Inf/tarca_600_clus_FP_gp_q}
        \caption{}
        \label{fig:tarca_600_clus_FP_gp_q}
    \end{subfigure}
    \bcaption{Assessing the effect of clustering on smaller gene sets (q-values)}{
        The same data as presented in \cref{fig:tarca_600_clus} is presented
        here but using q-values as a significance cutoff instead of p-values.
    }
    \label{fig:tarca_600_clus_q}
\end{figure}

\begin{figure}[bh!]
    \centering
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-MSigDB/MSigDB_clus_TP_q}
        \caption{}
        \label{fig:MSigDB_clus_TP_q}
    \end{subfigure}
    \begin{subfigure}[c]{0.32\textwidth}
        \includegraphics[height=4.8cm]{src/fig/BM-MSigDB/MSigDB_clus_FP_gp_q}
        \caption{}
        \label{fig:MSigDB_clus_FP_gp_q}
    \end{subfigure}
    \bcaption{Assessing the effect of clustering on \acrshort{MSigDB} data (q-values)}{
        The same data as presented in \cref{fig:MSigDB_clus} is presented
        here but using q-values as a significance cutoff instead of p-values.
    }
    \label{fig:MSigDB_clus_q}
\end{figure}

\FloatBarrier
\clearpage
% -----------------------------------------------------------------------------
\section{Module counts for \glspl{tpw} and non \glspl{tpw}}
\label{sec:module_counts_for_tpws_and_non_tpws}
% -----------------------------------------------------------------------------

\begin{figure}[bh!]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mcl_TP_freq_05}
        \caption{\gls{BinoX}, microarray data}
        \label{fig:Binox_mcl_TP_freq_05}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mcl_TP_freq_01}
        \caption{Ease, microarray data}
        \label{fig:Ease_mcl_TP_freq_01}
    \end{subfigure}

    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mcl_TP_MSigDb_freq_05}
        \caption{\gls{BinoX}, \gls{MSigDB} data}
        \label{fig:Binox_mcl_TP_MSigDb_freq_05}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mcl_TP_MSigDb_freq_01}
        \caption{Ease, \gls{MSigDB} data}
        \label{fig:Ease_mcl_TP_MSigDb_freq_01}
    \end{subfigure}
    \bcaption{
        Distribution of the number of significantly enriched modules using
        \acrshort{MCL}%
    }{
        After running the benchmark on both the microarray and \gls{MSigDB}
        data, the amount of modules made by \gls{MCL} that show a significant
        enrichment to a pathway was counted. In plot (\textbf{a}) for example,
        the gene sets obtained from the microarray data
        (\cref{sub:gold_standard_data}) were clustered using \gls{MCL} and then
        \gls{BinoX} was run on each module versus all \gls{KEGG} pathways.
        Shown in blue is the distribution of the number of modules that show
        significant enrichment for a gene set versus \gls{tpw} combination.  In
        pink, the same distribution is shown for gene set versus non \gls{tpw}
        combinations. Take plot (\textbf{b}) for example: for every \gls{qgs}
        there was only one module that was significantly enriched towards the
        \gls{tpw}. Since for all the \glspl{qgs} there was only $1$ significant
        module, the bar at $1$ is set to $100\%$ ( = $1.0$). And in plot
        (\textbf{c}) for example, about $45\%$ of the gene sets had only one
        module that was enriched to the target pathway, hence the blue bar at
        $1$ is set to $\pm45\%$. Modules in plots (\textbf{a}) and (\textbf{c})
        were counted as ``significant" if the \gls{BH} adjusted q-value from
        \gls{BinoX} was at least $0.05$. For plots (\textbf{b}) and
        (\textbf{d}), modules were ``significant" if the q-value from EASE was
        at least $0.10$.
    }
    \label{fig:modcount_mcl}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mgc_TP_freq_05}
        \caption{\gls{BinoX}, microarray data}
        \label{fig:Binox_mgc_TP_freq_05}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mgc_TP_freq_01}
        \caption{Ease, microarray data}
        \label{fig:Ease_mgc_TP_freq_01}
    \end{subfigure}

    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mgc_TP_MSigDb_freq_05}
        \caption{\gls{BinoX}, \gls{MSigDB} data}
        \label{fig:Binox_mgc_TP_MSigDb_freq_05}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mgc_TP_MSigDb_freq_01}
        \caption{Ease, \gls{MSigDB} data}
        \label{fig:Ease_mgc_TP_MSigDb_freq_01}
    \end{subfigure}
    \bcaption{
        Distribution of the number of significantly enriched modules using
        \acrshort{MGclus}%
    }{
        The gene sets from both benchmarks (microarray data and \gls{MSigDB}
        data) were clustered using \gls{MGclus} and the number of significant
        modules was counted. Module to \gls{tpw} combinations are shown in
        blue, all other combination are shown in pink. Modules in (\textbf{a})
        and (\textbf{c}) were counted as ``significant" if the q-value from
        \gls{BinoX} was at least $0.05$. Modules in (\textbf{b}) and
        (\textbf{d}) were counted as ``significant" if the q-value from Ease
        was at least $0.1$. See \cref{fig:modcount_mcl} for a more in-depth
        explanation.
    }
    \label{fig:modcount_mgc}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mcl_TP_freq_1}
        \caption{\gls{BinoX}, microarray data}
        \label{fig:Binox_mcl_TP_freq_1}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mcl_TP_freq_1}
        \caption{Ease, microarray data}
        \label{fig:Ease_mcl_TP_freq_1}
    \end{subfigure}

    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mcl_TP_MSigDb_freq_1}
        \caption{\gls{BinoX}, \gls{MSigDB} data}
        \label{fig:Binox_mcl_TP_MSigDb_freq_1}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mcl_TP_MSigDb_freq_1}
        \caption{Ease, \gls{MSigDB} data}
        \label{fig:Ease_mcl_TP_MSigDb_freq_1}
    \end{subfigure}
    \bcaption{
        Distribution of the number of modules with a p-value less than 1
        using \acrshort{MCL}%
    }{
        The gene sets from both benchmarks (microarray data and \gls{MSigDB}
        data) were clustered using \gls{MCL} and the number of modules that
        have an unadjusted p-value of less than one to a pathway were counted
        for \gls{BinoX} in (\textbf{a}) and (\textbf{c}) and for EASE in
        (\textbf{b}) and (\textbf{d}). In the case of \gls{BinoX}, the p-value
        for enrichment between a pathway and a module is less than 1 if there
        is at least one link between the module and the pathway. If there are
        no links, then the probability of seeing at least as many links under
        null hypothesis is equal to $1$. In the case of EASE the
        p-value is less than 1 if the overlap between a module and a pathway is
        at least two genes.  If the overlap is only one gene than $k-1$ in
        \cref{eq:ease_p} becomes zero, and the probability of seeing an overlap
        as extreme as zero is equal to $1$. If there is no overlap, then $k-1$
        will become $-1$ and the p-value is undefined. Module to \gls{tpw}
        combinations are shown in blue, all other combinations are shown in
        pink. Axes are explained in detail in \cref{fig:modcount_mcl}.
    }
    \label{fig:modcount_all_mcl}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mgc_TP_freq_1}
        \caption{\gls{BinoX}, microarray data}
        \label{fig:Binox_mgc_TP_freq_1}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mgc_TP_freq_1}
        \caption{Ease, microarray data}
        \label{fig:Ease_mgc_TP_freq_1}
    \end{subfigure}

    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Binox_mgc_TP_MSigDb_freq_1}
        \caption{\gls{BinoX}, \gls{MSigDB} data}
        \label{fig:Binox_mgc_TP_MSigDb_freq_1}
    \end{subfigure}%
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[]{src/fig/Clutsering_dists/Ease_mgc_TP_MSigDb_freq_1}
        \caption{Ease, \gls{MSigDB} data}
        \label{fig:Ease_mgc_TP_MSigDb_freq_1}
    \end{subfigure}
    \bcaption{
        Distribution of the number of modules with a p-value less than 1
        using \acrshort{MGclus}%
    }{
        The gene sets from both benchmarks (microarray data and \gls{MSigDB}
        data) were clustered using \gls{MGclus} and the number of modules that
        have an unadjusted p-value of less than one to a pathway were counted
        for \gls{BinoX} in (\textbf{a}) and (\textbf{c}) and for EASE in
        (\textbf{b}) and (\textbf{d}). Module to \gls{tpw} combinations are
        shown in blue, all other combinations are shown in pink.  For a more
        thorough explanation, see \cref{fig:modcount_all_mcl}. Axes are
        explained in detail in \cref{fig:modcount_mcl}.
    }
    \label{fig:modcount_all_mgc}
\end{figure}
