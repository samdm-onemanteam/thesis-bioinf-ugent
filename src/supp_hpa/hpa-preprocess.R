setwd("~/Documents/1_UGent/5_Uma2/2S-thesis/Tex")
setwd("src/supp_hpa")
library(readr)

KEGG <- read_tsv("raw_data/KEGG.tsv")
KEGG$GENE_ID <- NULL
KEGG <- KEGG[!duplicated(KEGG),]

Ease_u <- read_tsv("raw_data/EASE.tsv")
Ease_u$qval <- p.adjust(Ease_u$pVal, method = "BH")
Ease_u$clus <- FALSE
Ease_c <- read_tsv("raw_data/EASE-c.tsv")
Ease_c$qval <- p.adjust(Ease_c$pVal, method = "BH")
Ease_c$clus <- TRUE
Ease <- rbind(Ease_u, Ease_c)
Ease$tool <- "EASE"
Ease <- merge(Ease, KEGG[c("KEGG_uc_full", "KEGG_printname", "KEGG_ID")],
              by.x = "targetGeneSet", by.y = "KEGG_uc_full")
Ease$targetGeneSet <- NULL

BinoX_u <- read_tsv("raw_data/BinoX.tsv")
BinoX_u$clus <- FALSE
BinoX_c <- read_tsv("raw_data/BinoX-c.tsv")
BinoX_c$clus <- TRUE
BinoX <- rbind(BinoX_u, BinoX_c)
BinoX$tool <- "BinoX"
BinoX <- merge(BinoX, KEGG[c("KEGG_uc_full", "KEGG_printname", "KEGG_ID")],
               by.x = "NameGroupB", by.y = "KEGG_uc_full")
BinoX$qval <- BinoX$FDR
BinoX$FDR <- NULL
BinoX$NameGroupB <- NULL
BinoX$ID <- NULL
BinoX$id.first <- NULL
BinoX$id.second <- NULL
BinoX$pVal <- BinoX$p.value
BinoX$query <- BinoX$NameGroupA
BinoX$sizeA <- BinoX$groupSize.first
BinoX$p.value <- NULL
BinoX$NameGroupA <- NULL
BinoX$groupSize.first <- NULL

write_tsv(BinoX, "BinoX_preprocessed.tsv")
write_tsv(Ease,  "EASE_preprocessed.tsv")
